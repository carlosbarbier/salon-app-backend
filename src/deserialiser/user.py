from marshmallow_sqlalchemy import SQLAlchemyAutoSchema, auto_field, fields
from src.model import User


class UserSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = User
        load_instance = True
        exclude = ("created_at","updated_at","password",)

