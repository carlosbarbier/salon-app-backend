from flask_restful import Api

from src.resource.hello import HelloResource
from src.resource.user.register import UserRegisterResource


def register_routes(app):
    api = Api(app)
    api.add_resource(HelloResource, '/api/hello')
    api.add_resource(UserRegisterResource, '/api/users/register')
