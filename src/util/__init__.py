from werkzeug.security import generate_password_hash


def hash_password(password):
    return generate_password_hash(password)

def non_empty_string(s):
    if not s:
        raise ValueError("Must not be empty string")
    return s