
from sqlalchemy.exc import IntegrityError
from werkzeug.exceptions import InternalServerError, BadRequest, NotFound

from src.util import hash_password


class UserService:

    @staticmethod
    def save_user(args):
        from src.model import User
        user = User(name=args['name'], email=args['email'], password=hash_password(args['password']))
        try:
            user.save_to_db()
        except IntegrityError as e:
            raise BadRequest("email is taken")
        except Exception as e:
            raise InternalServerError("Internal Server")

    @staticmethod
    def find_user_by_email(email):
        from src.deserialiser import UserSchema
        from src.model import User
        user = User.query.filter_by(email=email).first()
        if user is not None:
            schema = UserSchema()
            return schema.dump(user)
        else:
            raise BadRequest("Invalid email or password")

    @staticmethod
    def verify_user(email, password):
        from src.deserialiser import UserSchema
        from src.model import User
        user = User.query.filter_by(email=email).first()
        if user is not None and user.verify_password(password):
            schema = UserSchema()
            return schema.dump(user)
        else:
            raise BadRequest("Invalid email or password")

    @staticmethod
    def confirm_email(args):
        token = args['token']
        user = UserService.__get_user_by_token(token)
        try:
            user.is_active = True
            user.token = None
            user.save_to_db()
        except Exception as ex:
            raise InternalServerError('Internal server error')





    @staticmethod
    def __get_user_by_email(e):
        from src.model import User
        try:
            user = User.find_by_first(email=e)
        except Exception as ex:
            raise InternalServerError('Internal server error')
        if user is not None:
            return user
        else:
            raise NotFound("user with this email not found")

    @staticmethod
    def __get_user_by_token(token):
        from src.model import User
        try:
            user = User.find_by_first(token=token)
        except Exception as ex:
            raise InternalServerError('Internal server error')
        if user is not None:
            return user
        else:
            raise NotFound("invalid or expired token")
