from sqlalchemy.orm import Query


class GetNullQasQuery(Query):
    def getQas(self):
        from src.model import StudentQa
        return self.filter(StudentQa.parent_id is None)
