from flask import Flask
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

import application
from src.config import config
from src.routes import register_routes


def create_app():
    app = Flask(__name__)

    # config properties
    app.config.from_object(config.get('development'))

    # database
    db.init_app(app)

    # migration
    migrate.init_app(app, db)
    from src.model import User

    # routes
    register_routes(app)

    return app


db = SQLAlchemy()
migrate = Migrate()
