from flask import jsonify
from flask_restful import Resource


class HelloResource(Resource):

    def get(self):
        return jsonify({"name": "Carlos"})
