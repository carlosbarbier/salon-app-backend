from flask_restful import Resource, reqparse

from src.service import UserService
from src.validation import user_registration_validator

parser = reqparse.RequestParser(bundle_errors=True)


class UserRegisterResource(Resource):
    def post(self):
        user_registration_validator(parser)
        args = parser.parse_args(http_error_code=422)
        UserService.save_user(args)
        return {}, 201

    def get(self):
        return UserService.find_user_by_email(email="kouakou@gmail.com")

